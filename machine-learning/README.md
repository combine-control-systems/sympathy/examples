## Advanced Machine Learning demo - Customer Churn 

### Dataset

Contains tabular data on contract and customer data (fictitious) with label (0/1) to indicate churn. The data is included here in the *data* folder.

### Clustering Analysis

ml_customer_segmentation_use_case.syx (Uses only standard Machine Learning library nodes): This demo illustrates how customer call and contract data can be combined to segment customers. 

ml_customer_segmentation_use_case_agg.syx (Requires Sympathy for Data Enterprise Version): This is the same flow as ml_customer_segmentation_use_case.syx but uses nodes specifically from the Enterprise version of Sympathy for Data. 


### Supervised Learning (Requires Sympathy for Data Enterprise Version)
This demo shows the basic ML pipeline construction for a Random Forest Classifier to predict whether a customer will churn or not (1/0). 