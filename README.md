# Examples

This repository contains multiple example flows that showcase key features for current and upcoming versions of Sympathy for Data. Please keep an eye on the requirements to ensure that the flows will run on your local setup. 

## Introduction to Sympathy for Data - Product Order Book
This demo uses fictitious order book data to illustrate key concepts for building flows from scratch in Sympathy For Data. Only the base version of Sympathy is required to run this flow. 

## Machine Learning demo - Churn 

### Clustering Analysis
This demo illustrates how customer call and contract data can be combined to segment customers. 

### Supervised Learning (Requires: Enterprise Version)
This demo shows the basic ML pipeline construction for a Random Forest Classifier to predict whether a customer will churn or not (1/0). 

## Image analysis

### Image binary classification demo (Requires: upcoming version 3.x of Sympathy for Data and Enterprise version)
  Example result:
  1. Confusion Matrix(numerical label)
  2. Confusion Matrix(group label)
  3. Precision, recall, F1 score and accuracy etc.
  4. Visualize images that have an error.

### Image data augmentation demo (Requires: Enterprise version)

The work uses the same example image dataset as classification demo and shows how people do image data augmentation in Sympathy.
* Data augmentation with standard library node single figure.
  e.g. Rotating, flipping, etc.
* Apply for all figures with Lambda.
* Other useful nodes for image data augmentation. e.g. "Color space conversion"

## Time Series Analysis

### Time series smoothing and anomaly detection (Requires: Enterprise version)

This demo uses multivariate time series data from an industrial setting to understand the degradation of a shrink wrap machine blade over time. Main techniques include filtering and smoothing. 

### Possible improvements advice on Sympathy nodes:
We are always open to suggestions for improvement.  
