## Image analysis


### Dataset

https://www.kaggle.com/ravirajsinh45/real-life-industrial-dataset-of-casting-product/version/2

| ![def](def_example.jpeg) | ![OK](ok_example.jpeg) |


### Image binary classification demo (note: This flow requires an upcoming version of Sympathy for Data 3.x) 

Binary Classification of industrial parts as OK (0) or DEFECTIVE (1) using upcoming lazy-loading feature. 

Examples of showcased functionality:
* Data Augmentation using Different perspectives
* Rotations
* Lazy-loading the data using Image Datasets

Example result:
  1. Confusion Matrix(numerical label)
  2. Confusion Matrix(group label)
  3. Precision, recall, F1 score and accuracy etc.
  4. Visualize misclassified images.

### Image data augmentation demo (Requires: Sympathy for Data Enterprise Version)

This flow uses the same example image dataset as classification demo and shows how to do image data augmentation in Sympathy for Data.

Examples of showcased functionality:

* Data augmentation with standard library node single figure.
  e.g. Rotating, flipping, etc.
* Apply for all figures with Lambda.
* Other useful nodes for image data augmentation. e.g. "Color space conversion"