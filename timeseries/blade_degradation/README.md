## Time Series Analysis

### Dataset

https://www.kaggle.com/inIT-OWL/one-year-industrial-component-degradation

### Use case description

time series demo.syx (Requires Sympathy for Data Enterprise Version): This demo uses multivariate time series data from an industrial setting to understand the degradation of a shrink wrap machine blade over time. Main techniques include filtering and smoothing. 
